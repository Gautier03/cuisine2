<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<link rel="stylesheet" href="css_files/footer.css">
<footer>
	<div class="contenu-footer">

		<div class="bloc-footer">
			<h3 class="titre-footer">Services</h3>
			<ul class="liste-service">
				<li><a href="#">Terms of use</a></li>
				<li><a href="#">Privacy </a></li>
				<li><a href="#">Legal </a></li>
			</ul>
		</div>
		<div class="bloc-footer">
			<h3 class="titre-footer">Restons en contact</h3>
			<ul class="liste-horaires">
				<li>Tél : 06060606060</li>
				<li>Mail : kern@mlv.fr</li>
				<li>Adresse : kekzeflk, IJBI 26351</li>
			</ul>
		</div>
		<div class="bloc-footer">
			<h3 class="titre-footer">Nos horaires</h3>
			<ul class="liste-horaires">
				<li>Lun / Ven 10h-19h</li>
				<li>Sam / Dim fermé</li>
			</ul>
		</div>

		<div class="bloc-footer">
			<h3 class="titre-footer">Nos réseau</h3>
			<ul class="liste-horaires">
				<li><a href="*" src="">Facebook</a></li>
				<li><a href="*" src="">Insta</a></li>
				<li><a href="*" src="">Twitter</a></li>
			</ul>
		</div>

	</div>

</footer>

</body>
</html>