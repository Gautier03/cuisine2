<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="header.jsp"%>

    <h1>Nos recettes</h1>
    <p>Ajoutez votre plat : </p>

    <form method="post">
        <p><label for="nom" id="nom">Nom</label>
        <input type="text" name="nom"></p><br> 
        <p><label for="description" id="description">Description</label> 
        <input type="text" name="description"></p><br> 
        <p><label for="classification" id="classification">Classification</label> 
        <select name="classification" id="classification">
        	<option value="APERITIF">APERITIF</option>
        	<option value="ENTREE">ENTREE</option>
        	<option value="PLAT">PLAT</option>
        	<option value="DESSERT">DESSERT</option>
        	<option value="BOISSON">BOISSON</option>
        </select></p><br> 
        <p><label for="prix" id="prix">Prix</label> 
        <input type="number" step="0.01" name="prix" value="0" required></p><br> 
        <input type="submit">
    </form>
    
    <p>${message}</p>
    
    	<table>
    		<tr><th>Nom</th><th>Description</th><th>Classification</th><th>Prix</th><th>Supprimer</th></tr>
    		
			<c:forEach items="${plats}" var="p">
			<tr>
				<td>${p.nom}</td>
				<td>${p.description}</td>
				<td>${p.classification}</td>
				<td>${p.prix}</td>
				<td><a href="<c:url value="/plats/supprimer"/>">DELETE</a>
				<form id="${p.id}" method="POST">
                    <input id="plat" name="plat" type="hidden" value="${plat.id}"/>
                </form></td>
			</tr>
			</c:forEach>
			
		</table>

<%@ include file="footer.jsp"%>