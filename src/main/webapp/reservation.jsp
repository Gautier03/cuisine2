<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="WEB-INF/vues/header.jsp"%>
<link rel="stylesheet" href="css_files/resa.css">

<h1>R�servation</h1>
<h2>Choisir la date et l'heure pour venir savourer vos plats pr�f�r�s
</h2>

<div class="resa">
<form method="post">
	
	<p>
		<label for="date">Date et heure</label> <input type="datetime-local"
			name="date" id="date" required>
	</p>
	<br>
	<p>
		<label for="nom">Nom</label> <input type="text" name="nom" id="nom" required>
	</p>
	<br>
	<p>
		<label for="nbPersonnes">Nombre de personnes</label> <input
			type="number" step="1" min=1 max=10 name="nbPersonnes" value="0" required
			id="nbPersonnes">
	</p>
	<br> <input class="btn" type="submit" id="btLire">
</form>
</div>

<table id="table">
</table>

<script>
	// r�cup�ration des donn�es inscrites
    let btLire = document.getElementById("btLire")
    let nom = document.getElementById("nom")
    let date = document.getElementById("date")
    let nbPersonnes = document.getElementById("nbPersonnes")
    let tableResas = document.getElementById("table")
    
    // supprime une r�servation
    function supprime(id){
    	
        let xhrSuppr = new XMLHttpRequest()
        xhrSuppr.open("DELETE","http://localhost:8082/reservation?id="+id);
        xhrSuppr.send();
        
        // mise � jour de la table
        xhrSuppr.onreadystatechange = function() {
        	getReservations() 
        }
        
    }
    
    // r�cup�re les reservations dans notre base de donn�es
    function getReservations() {
    
   		let xhr = new XMLHttpRequest()
        xhr.open("GET","http://localhost:8082/reservation")
        xhr.onreadystatechange = function () {
        
        if(this.status == 200 && this.readyState==4){
             console.log("R�ponse re�ue : "+this.responseText)
             let resas = JSON.parse(this.responseText)
             let html = "<tr>"
             html += "<th>Nom</th>"
             html += "<th>Date et heure</th>"
             html += "<th>Nombre de personnes</th>"
             html += "<th>Supprimer</th>"
             html += " </tr>"
             for (let reservation of resas) {
                  html += "<tr><td>"+reservation["nom"]+"</td>"
                  html += "<td>"+reservation["date"]+"</td>"
                  html += "<td>"+reservation["nbPersonnes"]+"</td>"
                  html += '<td><button onclick="supprime('+reservation["id"]+')">x</button></td></tr>'
             }
             tableResas.innerHTML = html
         }
      }
      xhr.send();
    }
    getReservations() // affiche les r�servations au d�marrage
    
    // bouton ajout d'une r�servations
    btLire.addEventListener("click", function() {
       		
        let xhrPost = new XMLHttpRequest()
        xhrPost.open("POST","http://localhost:8082/reservation");
        xhrPost.setRequestHeader("Content-Type","application/json;charset=UTF-8");
        xhrPost.send(JSON.stringify({"date":date.value,"nom":nom.value,"nbPersonnes":nbPersonnes.value}));
        
        // mise � jour de la table une fois que l'ajout est termin�
        xhrPost.onreadystatechange = function() {
        	getReservations() 
        }
       
    })
    
    </script>

<%@ include file="WEB-INF/vues/footer.jsp"%>