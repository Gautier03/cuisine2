package fr.ib.cuisine;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller @Lazy
public class CuisineController {

    private SessionFactory sessionFactory;

    // affichage des plats dans la jsp
    @RequestMapping(path="/plats", method=RequestMethod.GET)
    public String afficher(Model model) {

	Session session = sessionFactory.openSession();
	List<Plat> plats = session.createQuery("from Plat",Plat.class).list();
	session.close();
	model.addAttribute("plats", plats);

	return "plats";

    }

    // ajout d'un plat à la table
    @RequestMapping(path="/plats", method=RequestMethod.POST)
    public String traiterFormulaire(
	    @RequestParam("nom") String nom,
	    @RequestParam("description") String description,
	    @RequestParam("classification") EtatClassification classification,
	    @RequestParam("prix") double prix,
	    Model model) {
	if(nom.trim().length()<2 || description.trim().length()<20 || prix<0 || prix>=1000) {
	    model.addAttribute("message", "Erreur dans le formulaire");
	}
	else {
	    Session session = sessionFactory.openSession();
	    Plat p1 = new Plat(nom, description, classification, prix);
	    session.save(p1);
	    session.close();
	}
	return afficher(model);  

    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
	this.sessionFactory = sessionFactory;
    }


}
