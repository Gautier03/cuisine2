package fr.ib.cuisine;

import java.time.LocalDateTime;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReservationController {

    private SessionFactory sessionFactory;

    // affichage des reservation dans la jsp
    @RequestMapping(path="/reservation", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Reservation> getTous() {

	Session session = sessionFactory.openSession();
	List<Reservation> reservations = session.createQuery("from Reservation", Reservation.class).list();
	session.close();
	System.out.println(reservations);
	return reservations;
    }

    // ajout d'une réservation
    @RequestMapping(path="/reservation", method=RequestMethod.POST)
    public boolean traiterFormulaire(
	    @RequestBody Reservation resa)
    {
	System.out.println(resa);
	if(resa.getNom().trim().length()<2 || resa.getDate().isBefore(LocalDateTime.now())) {
	    return false;
	}
	else {
	    Session session = sessionFactory.openSession();
	    session.save(resa);
	    session.close();
	    System.out.println(resa);
	}
	return true;
    }

    // suppression d'une réservation
    @RequestMapping(path="/reservation",  method=RequestMethod.DELETE)
    public void supprimeAvecId(@RequestParam("id") int id) {
	Session session = sessionFactory.openSession();
	Transaction tx = session.beginTransaction();
	Reservation resa = session.get(Reservation.class, id);
	session.delete(resa);
	tx.commit();
	session.close();
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
	this.sessionFactory = sessionFactory;
    }


}
