package fr.ib.cuisine.clients;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import fr.ib.cuisine.Reservation;


public class ClientSpring {
	
	public static void main(String[] args) {
	

	try {
		System.out.println("Client Java");
		
		RestTemplate rt = new RestTemplate();
		//getForObject convertie tout en chaine de caractère
		String health = rt.getForObject("http://localhost:8082/reservation", String.class);	
		
		Integer nbCd = rt.getForObject("http://localhost:8082/reservation", Integer.class);
						
		//on crée une nouvelle réservation
		Reservation resa = new Reservation(LocalDateTime.now(), "nom", 0);
		rt.postForObject("http://localhost:8082/reservation", resa, Void.class); //void : méthode qui n'attend pas de resultat
		
		
		//solution 1 pour afficher la liste des cd
		//ParameterizedTypeReference<List<Cd>> ref = new ParameterizedTypeReference<List<Cd>>() {};
		//ResponseEntity<List<Cd>> cdsEntity = rt.exchange("http://localhost:9002/cd", HttpMethod.GET, null, ref);
		//List<Cd>cds = cdsEntity.getBody();
		//for (Cd cd:cds) {
			//System.out.println(" - "+cd);
		//}
		
		//solution 2
		Reservation[] resas = rt.getForObject("http://localhost:8082/reservation", Reservation[].class);
		System.out.println("Tous les reservations : ");
		
		
		//Modifier le titre du cd
		
		// rt.put("http://localhost:9002/cd/0/titre", "help!", String.class);
		
	
	} catch (Exception ex) {
		System.err.println("Erreur : "+ex);
		}
	}
}

