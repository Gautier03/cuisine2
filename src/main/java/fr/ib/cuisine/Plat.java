package fr.ib.cuisine;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="plat")
public class Plat implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id = 0;
	private String nom;
	private String description;
	private EtatClassification classification;
	private double prix;
	
	public Plat(String nom, String description, EtatClassification classification, double prix) {
		this.nom = nom;
		this.description = description;
		this.classification = classification;
		this.prix = prix;
	}
	
	public Plat() {
		this(null, null, null, 0);
	}

	@Column(name="id")
	@Id //clé primaire : unique, généré automatiquement
	@GeneratedValue (strategy=GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	@Column(length=50, nullable=false, unique=false) // plusieurs recette pour un meme plat
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@Column(length=500, nullable=false, unique=false)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(nullable=false, unique=false)
	public EtatClassification getClassification() {
		return classification;
	}

	public void setClassification(EtatClassification classification) {
		this.classification = classification;
	}
	@Column(length=50, precision=5, scale=2)
	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "Plat [id=" + id + ", nom=" + nom + ", description=" + description + ", classification=" + classification
				+ ", prix=" + prix + "]";
	}
	
	
	
	
	
	
	
	
	

}
