package fr.ib.cuisine;

// liste déroulante description du plat
public enum EtatClassification {
    APERITIF, ENTREE, PLAT, DESSERT, BOISSON, ACCOMPAGNEMENT
}
