package fr.ib.cuisine;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="reservation")
public class Reservation implements Serializable {


    private static final long serialVersionUID = 1L;

    private int id = 0;
    private LocalDateTime date;
    private String nom;
    private int nbPersonnes;

    public Reservation(LocalDateTime date, String nom, int nbPersonnes) {
	this.date = date;
	this.nom = nom;
	this.nbPersonnes = nbPersonnes;
    }

    public Reservation() {
	super();
    }

    @Column(name="id")
    @Id //clé primaire : unique, généré automatiquement
    @GeneratedValue (strategy=GenerationType.IDENTITY)
    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    @Column(unique=false)
    public LocalDateTime getDate() {
	return date;
    }

    public void setDate(LocalDateTime date) {
	this.date = date;
    }

    @Column(length=50, nullable=false, unique=false)
    public String getNom() {
	return nom;
    }

    public void setNom(String nom) {
	this.nom = nom;
    }

    @Column
    public int getNbPersonnes() {
	return nbPersonnes;
    }

    public void setNbPersonnes(int nbPersonnes) {
	this.nbPersonnes = nbPersonnes;
    }

    @Override
    public String toString() {
	return "Reservation [date=" + date + ", nom=" + nom + ", nbPersonnes=" + nbPersonnes + "]";
    }






}
